# Contributing

Ideas are always welcome, so if you think something is missing or it can be
improved somehow, feel free to [open an issue](https://gitlab.com/jsmanrique/my-bootstrap-jekyll-base/issues/new).