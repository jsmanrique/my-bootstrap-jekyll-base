# My Bootstrap Jekyll base

A Bootstrap base site generated with Jekyll to ease simple webs creation and 
prototyping

## Rationale

From time to time, I need to generate a website for multiple reasons:
* mockup a new service
* quick set up of a conference website
* mockup a new website to ease its information architecture
* to test some web APIs or some integrations

# How to use it?

The goal is to have something as simple as:
1. [Downaload it](https://gitlab.com/jsmanrique/my-bootstrap-jekyll-base/-/archive/master/my-bootstrap-jekyll-base-master.zip) [zip file]
2. Customize it for your own needs

As an example, [this is how looks like](https://jsmanrique.gitlab.io/my-bootstrap-jekyll-base/) 
the content of this repository if you deploy it as it is.

## How to run or test the site in your own laptop/computer/etc.?

You need [Ruby](https://www.ruby-lang.org/) installed in your device.

Inside your working folder, you should do:
```
$ bundle install
```

In some systems this raise an error due to permissions. To avoid running it as
root, it seems this works better:
```
$ bundle install --path vendor/bundle
```

This will install [Jekyll](https://jekyllrb.com/) and some other needed pieces 
to be used later to build your site locally (in your laptop/computer/etc.)- Once
it's over, to build and serve the site, you should run:
```
$ bundle exec jekyll serve
```

Your site should be available in `http://localhost:4000`

## How to run or test the site in GitHub?

If you upload your customized site to a GitHub repository, it should be available
thanks to [GitHub Pages](https://guides.github.com/features/pages/) support out-of-the-box in:
* `https://[YOUR-GITHUB-USERNAME].github.io/` if you push it to a repository named `[YOUR-GITHUB-USERNAME].github.io` 
* or `https://[YOUR-GITHUB-USERNAME].github.io/[REPOSITORY-NAME]` if you push it to another `[REPOSITORY-NAME]`

## How to run or test the site in GitLab?

If you upload your customized site to a GitLub repository, it should be available
thanks to [GitLab Pages](https://about.gitlab.com/product/pages/) support out-of-the-box in:
* `https://[YOUR-GITHUB-USERNAME].gitlabb.io/` if you push it to a repository named `[YOUR-GITHUB-USERNAME].github.io` 
* or `https://[YOUR-GITHUB-USERNAME].gitlab.io/[REPOSITORY-NAME]` if you push it to another `[REPOSITORY-NAME]`

# How this has been built?

This is the recipe I've followed to build this base site:

1. Create a minimal Jekyll site required files and folders: [`_config.yml`](_config.yml), `_layouts`, `_includes` and `_sass`
2. Download  [Bootstrap](https://getbootstrap.com/) source code
3. Copy all files under `scss` into [`_sass/bootstrap/`](_sass/bootstrap/)
4. You need a [`css/main.scss`](css/main.scss) to include those files and to be able to add your custom styles
5. You need a [`_includes/js.html`](_includes/js.html) to include required .js files from CDNs
6. Write some basic [`_includes/head.html`](_includes/head.html), and [`_layouts/default.html`](_layouts/default.html) files
7. Create a basic [`index.html`](index.html) file (page)

# Contributing

Ideas are always welcome, so if you think something is missing or it can be
improved somehow, feel free to [open an issue](https://gitlab.com/jsmanrique/my-bootstrap-jekyll-base/issues/new).

# License

This work is being done under [MIT license](LICENSE)